package com.app.submission.mvc.models.request;

public class ResolveRequest {
    private String account_id;
    private int problem_id;
    private String source_code;
    private Integer language_id;
    private String stdin;
    private String expected_output;
    private Float cpu_time_limit;
    private Integer memory_limit;

    public ResolveRequest() {
    }

    public ResolveRequest(String account_id, int problem_id, String source_code,
                          Integer language_id, String stdin, String expected_output,
                          Float cpu_time_limit, Integer memory_limit) {
        this.account_id = account_id;
        this.problem_id = problem_id;
        this.source_code = source_code;
        this.language_id = language_id;
        this.stdin = stdin;
        this.expected_output = expected_output;
        this.cpu_time_limit = cpu_time_limit;
        this.memory_limit = memory_limit;
    }

    public String getAccount_id() {
        return account_id;
    }

    public void setAccount_id(String account_id) {
        this.account_id = account_id;
    }

    public int getProblem_id() {
        return problem_id;
    }

    public void setProblem_id(int problem_id) {
        this.problem_id = problem_id;
    }

    public String getSource_code() {
        return source_code;
    }

    public void setSource_code(String source_code) {
        this.source_code = source_code;
    }

    public Integer getLanguage_id() {
        return language_id;
    }

    public void setLanguage_id(Integer language_id) {
        this.language_id = language_id;
    }

    public String getStdin() {
        return stdin;
    }

    public void setStdin(String stdin) {
        this.stdin = stdin;
    }

    public String getExpected_output() {
        return expected_output;
    }

    public void setExpected_output(String expected_output) {
        this.expected_output = expected_output;
    }

    public Float getCpu_time_limit() {
        return cpu_time_limit;
    }

    public void setCpu_time_limit(Float cpu_time_limit) {
        this.cpu_time_limit = cpu_time_limit;
    }

    public Integer getMemory_limit() {
        return memory_limit;
    }

    public void setMemory_limit(Integer memory_limit) {
        this.memory_limit = memory_limit;
    }
}
