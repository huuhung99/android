package com.app.submission.mvc.views;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.app.submission.R;
import com.app.submission.mvc.controllers.API;
import com.app.submission.mvc.models.User;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class RankingFragment extends Fragment {

    private int idUser;
    private RecyclerView _listRankingView;
    private List<UserRanking> _ranking;
    private MyRankRecyclerViewAdapter adapter;
    private TextView _top1Name , _top1Point , _top2Name,_top2Point , _top3Name , _top3Point;
    public RankingFragment(int idUser) {
        this.idUser = idUser;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_ranking, container, false);
        _listRankingView = root.findViewById(R.id.rv_ranking);
        _top1Name = root.findViewById(R.id.top1name);
        _top2Name = root.findViewById(R.id.top2name);
        _top3Name = root.findViewById(R.id.top3name);
        _top1Point = root.findViewById(R.id.top1point);
        _top2Point = root.findViewById(R.id.top2point);
        _top3Point = root.findViewById(R.id.top3point);
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        _ranking = new ArrayList<>();




        AndroidNetworking.get(API.LOCAL + "resolve/ranking")
                .build().getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray array = response.getJSONArray("items");
                    for (int i = 0 ; i < array.length() ; i++){
                        JSONObject obj = array.getJSONObject(i);
                        _ranking.add(new UserRanking(obj.getString("user_name"),obj.getInt("sum")));
                    }
                    if(_ranking.size() == 0){
                        _top1Name.setText("");
                        _top1Point.setText("");
                        _top2Name.setText("");
                        _top2Point.setText("");
                        _top3Name.setText("");
                        _top3Point.setText("");
                    }
                    if(_ranking.size() == 1){
                        _top1Name.setText(_ranking.get(0).getName());
                        _top1Point.setText(_ranking.get(0).getPoint()+"");
                        _top2Name.setText("");
                        _top2Point.setText("");
                        _top3Name.setText("");
                        _top3Point.setText("");
                    }
                    if(_ranking.size() == 2){
                        _top1Name.setText(_ranking.get(0).getName());
                        _top1Point.setText(_ranking.get(0).getPoint()+"");
                        _top2Name.setText(_ranking.get(1).getName());
                        _top2Point.setText(_ranking.get(1).getPoint()+"");
                        _top3Name.setText("");
                        _top3Point.setText("");
                    }
                    if(_ranking.size() >=3){
                        _top1Name.setText(_ranking.get(0).getName());
                        _top1Point.setText(_ranking.get(0).getPoint()+"");
                        _top2Name.setText(_ranking.get(1).getName());
                        _top2Point.setText(_ranking.get(1).getPoint()+"");
                        _top3Name.setText(_ranking.get(2).getName());
                        _top3Point.setText(_ranking.get(2).getPoint()+"");
                    }


                    adapter = new MyRankRecyclerViewAdapter(getContext() , _ranking);
                    _listRankingView.setAdapter(adapter);
                    _listRankingView.setLayoutManager(new LinearLayoutManager(getContext()));
                } catch (Exception e){
                    e.printStackTrace();

                }
            }
            @Override
            public void onError(ANError anError) {

            }
        });

    }


}
class UserRanking{
    private String name;
    private int point;
    public UserRanking(String name , int point){this.name=name; this.point = point;}
    public void setName(String name){
        this.name = name;
    }
    public String getName(){return this.name;}
    public void setPoint(int point) {
        this.point = point;
    }
    public int getPoint(){return this.point;}
}
class MyRankRecyclerViewAdapter extends RecyclerView.Adapter<MyRankRecyclerViewAdapter.MyRankViewHolder> {
    private Context _context;
    private List<UserRanking> _ranking;

    public MyRankRecyclerViewAdapter(Context context , List<UserRanking> ranking) {
        this._context = context;
        this._ranking = ranking;
    }

    @NonNull
    @Override
    public MyRankViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(_context);
        View view = inflater.inflate(R.layout.row_item_user, parent, false);
        return new MyRankRecyclerViewAdapter.MyRankViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyRankViewHolder holder, int position) {
        UserRanking ur = _ranking.get(position);
        holder._nameUser.setText(ur.getName());
        holder._pointUser.setText(ur.getPoint()+"");
    }

    @Override
    public int getItemCount() {
        return _ranking.size();
    }

    public class MyRankViewHolder extends RecyclerView.ViewHolder{
        private TextView _nameUser;
        private TextView _pointUser;
        public MyRankViewHolder(@NonNull View itemView) {
            super(itemView);
            _nameUser = itemView.findViewById(R.id.name);
            _pointUser = itemView.findViewById(R.id.point);
        }
    }
}

