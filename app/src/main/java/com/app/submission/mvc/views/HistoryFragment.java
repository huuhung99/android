package com.app.submission.mvc.views;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.app.submission.mvc.models.ItemAdapter;
import com.app.submission.R;
import com.app.submission.mvc.controllers.API;
import com.app.submission.mvc.models.Problem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class HistoryFragment extends Fragment implements ItemAdapter.ItemClickListener {
    private RecyclerView rvHistory;
    private ItemAdapter adapter;
    private ArrayList<Problem> list= new ArrayList<>();
    private MainActivity main;
    private int idUser;

    public HistoryFragment(int idUser) {
        this.idUser = idUser;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_history, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();

    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if(menuVisible){
            loadData();
        }
    }

    private void initView() {
        main= new MainActivity();
        rvHistory= getActivity().findViewById(R.id.rv_history);
        adapter= new ItemAdapter(getLayoutInflater());
        adapter.setListener(this);
        loadData();

    }
    public void loadData(){
        
        AndroidNetworking.get(API.LOCAL+"resolve/user/{userId}")
                .addPathParameter("userId", idUser+"")
                .build().getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray jsonArray= response.getJSONArray("items");
                    for (int i=jsonArray.length()-1; i>= 0; i--){
                        JSONObject jsonObject= jsonArray.getJSONObject(i);
                        String status= jsonObject.getString("status");
                        JSONObject problemObj= jsonObject.getJSONObject("problem");
                        int id= Integer.parseInt(jsonObject.getString("id"));
                        String title= problemObj.getString("title");
                        String topic= problemObj.getString("topic");
                        String level= problemObj.getString("level");
                        Problem problem= new Problem(id, title, topic, level);
                        problem.setSolved(status);
                        list.add(problem);
                        adapter.setList(list);
                        rvHistory.setAdapter(adapter);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(ANError anError) {

            }
        });

    }

    @Override
    public void onItemClick(Problem problem) {
        AndroidNetworking.get(API.LOCAL + "resolve/"+problem.getId())
                .addHeaders("content-type","application/json")
                .build().getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    String code= response.getString("code");
                    String message = response.getString("message");

                    if(code.equals("R_200") && message.equals("OK")){
                        JSONObject resolve = response.getJSONObject("item");
                        String status = resolve.getString("status").equals("null") ? "" : resolve.getString("status");
                        String complie = resolve.getString("complieOutput").equals("null") ? "" : resolve.getString("complieOutput");
                        Toast.makeText(getContext() , status + " " + complie , Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
            @Override
            public void onError(ANError anError) {

            }
        });
    }
}