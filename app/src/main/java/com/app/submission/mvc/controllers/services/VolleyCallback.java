package com.app.submission.mvc.controllers.services;

public interface VolleyCallback {
    void onSuccess(String result);
}
