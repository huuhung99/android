package com.app.submission.mvc.views;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.app.Activity;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.app.submission.R;
import com.app.submission.mvc.controllers.ClientControl;
import com.app.submission.mvc.models.DataPackage;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    private ListProblemFragment listProblemFragment;
    private RankingFragment rankingFragment;
    private HistoryFragment historyFragment;
    private ProfileFragment profileFragment;
    private int currentFragment=1;
    private boolean flag = false;
    private int _idUser;
    private Bundle bundle;

    private BottomNavigationView bottomNavigationView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        _idUser = Integer.parseInt(getIntent().getStringExtra("userId"));
        flag = getIntent().getBooleanExtra("fromCode",false);
        listProblemFragment = new ListProblemFragment(_idUser);
        rankingFragment= new RankingFragment(_idUser);
        historyFragment= new HistoryFragment(_idUser);
        profileFragment= new ProfileFragment(_idUser);

        setContentView(R.layout.activity_main);
        initView();
    }
    public void initView(){
        transparentStatusBar();

        bottomNavigationView= findViewById(R.id.bottomNavigationView);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
        initFragment();
        showFragment(listProblemFragment);
        if(flag){
            showFragment(historyFragment);
        }
    }

    private void transparentStatusBar() {
        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true);
        }
        if (Build.VERSION.SDK_INT >= 19) {
            this.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        //make fully Android Transparent Status bar
        if (Build.VERSION.SDK_INT >= 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
            this.getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
    }
    public static void setWindowFlag(Activity activity, final int bits, boolean on) {
        Window win = activity.getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }

    public void initFragment(){
        FragmentTransaction transaction= getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.frame_layout, listProblemFragment);
        transaction.add(R.id.frame_layout, rankingFragment);
        transaction.add(R.id.frame_layout, historyFragment);
        transaction.add(R.id.frame_layout, profileFragment);
        transaction.commit();
    }
    public void showFragment(Fragment fragment){
        FragmentTransaction transaction= getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        transaction.hide(listProblemFragment);
        transaction.hide(rankingFragment);
        transaction.hide(historyFragment);
        transaction.hide(profileFragment);
        transaction.show(fragment);
        transaction.commit();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.nav_listProblem:
                if(currentFragment!=1){
                    showFragment(listProblemFragment);
                    currentFragment=1;
                }
                break;
            case R.id.nav_ranking:
                if(currentFragment!=2){
                    showFragment(rankingFragment);
                    currentFragment=2;
                }
                break;
            case R.id.nav_history:
                if(currentFragment!=3){
                    showFragment(historyFragment);
                    currentFragment=3;
                }
                break;
            case R.id.nav_profile:
                if(currentFragment!=4){
                    showFragment(profileFragment);
                    currentFragment=4;
                }
                break;
        }
        return true;
    }

    public Bundle getMyBundle() {
        bundle= getIntent().getExtras();
        return bundle;
    }
}