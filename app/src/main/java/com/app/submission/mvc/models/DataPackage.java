package com.app.submission.mvc.models;

import java.io.Serializable;

public class DataPackage implements Serializable {
    private String key ;
    private String value;

    public DataPackage() {
    }

    public DataPackage(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
