package com.app.submission.mvc.views;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.text.Html;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.app.submission.R;
import com.app.submission.mvc.controllers.API;
import com.app.submission.mvc.controllers.ProblemController;

import org.json.JSONArray;
import org.json.JSONObject;

public class DetailSingleProblemFragment extends Fragment {
    private ListView _exampleListView;
    private TextView _titleProblem,_contentProblem,_inputProblem,_outputProblem,_noteProblem;
    private ProblemController _problemController;
    private int _idProblem;
    public DetailSingleProblemFragment(int idProblem){
        this._idProblem = idProblem;
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_detail_single_problem, container, false);
        _exampleListView = root.findViewById(R.id.example_value);
        _inputProblem = root.findViewById(R.id.input_value);
        _titleProblem = root.findViewById(R.id.title_value);
        _contentProblem = root.findViewById(R.id.description_value);
        _outputProblem = root.findViewById(R.id.output_value);
        _noteProblem = root.findViewById(R.id.note_value);
        return root;
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        _problemController =  new ProblemController(this.getContext());
        
        
        _problemController.set_url(API.LOCAL+"problem/" + _idProblem);
        _problemController.createGetRequestApi(result -> {
            try {
                JSONObject api = new JSONObject(result);
                JSONObject obj = api.getJSONObject("item");
                _titleProblem.setText(Html.fromHtml(new String(obj.getString("title").getBytes("ISO-8859-1"),"UTF-8")));
                _contentProblem.setText(Html.fromHtml(new String(obj.getString("content").getBytes("ISO-8859-1"),"UTF-8")));
                _inputProblem.setText(Html.fromHtml(new String(obj.getString("input").getBytes("ISO-8859-1"),"UTF-8")));
                _outputProblem.setText(Html.fromHtml(new String(obj.getString("output").getBytes("ISO-8859-1"),"UTF-8")));
                _noteProblem.setText(R.string.note_value);
                String[] input = new String[]{};
                input[0] = obj.getString("input");

                String[] output = new String[]{};
                output[0] = obj.getString("output");
                MyListViewAdapter adapter = new MyListViewAdapter(this.getContext() , input , output);
                _exampleListView.setAdapter(adapter);
            }catch(Exception e){
                System.out.println(e.getStackTrace());
            }
        });
    }
}
class MyListViewAdapter extends BaseAdapter{
    private Context _context;
    private String[] _input;
    private String[] _output;
    public MyListViewAdapter(Context context , String[] input , String[] output){
        _context = context;
        _input = input;
        _output = output;
    }
    @Override
    public int getCount() {
        return _input.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) _context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.row_item_testcase,null);//layout file for displaying items
        TextView input = convertView.findViewById(R.id.input_value_example);
        TextView output = convertView.findViewById(R.id.output_value_example);
        input.setText(_input[position]);
        output.setText(_output[position]);
        return convertView;
    }
}
class NonScrollListView extends ListView {

    public NonScrollListView(Context context) {
        super(context);
    }
    public NonScrollListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    public NonScrollListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int heightMeasureSpec_custom = View.MeasureSpec.makeMeasureSpec(
                Integer.MAX_VALUE >> 2, View.MeasureSpec.AT_MOST);
        super.onMeasure(widthMeasureSpec, heightMeasureSpec_custom);
        ViewGroup.LayoutParams params = getLayoutParams();
        params.height = getMeasuredHeight();
    }
}