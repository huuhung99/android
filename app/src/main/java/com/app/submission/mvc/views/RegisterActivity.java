package com.app.submission.mvc.views;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.app.submission.R;
import com.app.submission.mvc.controllers.API;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String USERNAME_EXTRA = "USERNAME_EXTRA";
    public static final String PASSWORD_EXTRA = "PASSWORD_EXTRA";

    private EditText edtUsername;
    private EditText edtPassword;
    private EditText edtConfirmPassword;
    private RadioButton radMale;
    private RadioButton radFemale;
    private EditText edtName;

    private Button btnRegister;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        initView();
    }

    private void initView() {
        edtUsername= findViewById(R.id.edt_register_username);
        edtConfirmPassword= findViewById(R.id.edt_register_confirmPassword);
        edtPassword= findViewById(R.id.edt_register_password);
        edtName= findViewById(R.id.edt_register_name);
        radMale= findViewById(R.id.rad_male);
        radFemale= findViewById(R.id.rad_female);
        btnRegister= findViewById(R.id.btn_register);
        btnRegister.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        String username= edtUsername.getText().toString();
        String password= edtPassword.getText().toString();
        String password_= edtConfirmPassword.getText().toString();
        String name= edtName.getText().toString();
        String sex= (radMale.isChecked()) ? "Nam" : "Nữ";


        if(validate(username, password, password_, name)){
            int index= name.lastIndexOf(" ");
            String lastName= name.substring(index+1) + "";
            String firstName= name.substring(0,index) +"";
            JSONObject jsonObject= new JSONObject();
            try {
                jsonObject.put("user_name", username);
                jsonObject.put("password", password);
                jsonObject.put("first_name", firstName);
                jsonObject.put("last_name", lastName);
                jsonObject.put("sex", sex);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            AndroidNetworking.post(API.LOCAL+"user").addJSONObjectBody(jsonObject).setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                @Override
                public void onResponse(JSONObject response) {

                    try {
                        String code= response.getString("code");
                        String message= response.getString("message");
                        if(code.equals("R_201") && message.equals("CREATE")){
                            Toast.makeText(RegisterActivity.this, "Register Successfully!", Toast.LENGTH_SHORT).show();
                            Intent intent= new Intent();
                            intent.putExtra(USERNAME_EXTRA, edtUsername.getText().toString());
                            intent.putExtra(PASSWORD_EXTRA, edtPassword.getText().toString());
                            setResult(RESULT_OK, intent);
                            finish();
                        } else {
                            Toast.makeText(RegisterActivity.this, "Register Failed!", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onError(ANError anError) {
                    Toast.makeText(RegisterActivity.this , "Lost Connection To Server",Toast.LENGTH_SHORT).show();
                }
            });
        } else{
            Toast.makeText(this, "Invalid!", Toast.LENGTH_SHORT).show();
        }
    }

    public boolean validate(String username, String password, String password_, String name){

        if (username.isEmpty() || password.isEmpty() || !password.equals(password_) || name.isEmpty()){
            return false;
        }
        return true;
    }
}