package com.app.submission.mvc.models.request;

public class ProblemRepuest {
    private int id;
    private String input;
    private String output;
    private boolean active;
    private String code;
    private String title;
    private String topic;
    private String content;
    private String level;

    public ProblemRepuest() {
    }

    public ProblemRepuest(int id, String input, String output, boolean active, String code,
                          String title, String topic, String content, String level) {
        this.id = id;
        this.input = input;
        this.output = output;
        this.active = active;
        this.code = code;
        this.title = title;
        this.topic = topic;
        this.content = content;
        this.level = level;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }
}
