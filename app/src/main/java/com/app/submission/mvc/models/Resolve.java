package com.app.submission.mvc.models;

import java.util.Date;
public class Resolve {
    private int id;
    private String accountId;
    private Problem problem;
    private String status;
    private Float time;
    private Date submissionTime;
    private Integer memory;
    private String token;
    private String complieOutput;
    private String massage;
    private Language language;

    public Resolve() {
    }

    public Resolve(int id, String accountId, Problem problem, String status, Float time,
                   Date submissionTime, Integer memory, String token, String complieOutput,
                   String massage, Language language) {
        this.id = id;
        this.accountId = accountId;
        this.problem = problem;
        this.status = status;
        this.time = time;
        this.submissionTime = submissionTime;
        this.memory = memory;
        this.token = token;
        this.complieOutput = complieOutput;
        this.massage = massage;
        this.language = language;
    }
    public Resolve(int id ,Problem problem , String status , String accountId){
        this.id = id;
        this.accountId = accountId;
        this.problem = problem;
        this.status = status;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public Problem getProblem() {
        return problem;
    }

    public void setProblem(Problem problem) {
        this.problem = problem;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Float getTime() {
        return time;
    }

    public void setTime(Float time) {
        this.time = time;
    }

    public Date getSubmissionTime() {
        return submissionTime;
    }

    public void setSubmissionTime(Date submissionTime) {
        this.submissionTime = submissionTime;
    }

    public Integer getMemory() {
        return memory;
    }

    public void setMemory(Integer memory) {
        this.memory = memory;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getComplieOutput() {
        return complieOutput;
    }

    public void setComplieOutput(String complieOutput) {
        this.complieOutput = complieOutput;
    }

    public String getMassage() {
        return massage;
    }

    public void setMassage(String massage) {
        this.massage = massage;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }
}
