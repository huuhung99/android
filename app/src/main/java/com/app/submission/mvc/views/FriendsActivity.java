package com.app.submission.mvc.views;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.app.submission.R;
import com.app.submission.mvc.controllers.API;
import com.app.submission.mvc.controllers.ClientControl;
import com.app.submission.mvc.models.DataPackage;
import com.app.submission.mvc.models.User;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class FriendsActivity extends AppCompatActivity {
    private SearchView searchFriendsView;
    private RecyclerView listFriendsView;
    private List<User> _listFriends = new ArrayList<>();
    private MyFriendsRecyclerViewAdapter adapter;
    private String _idOwner;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_friends);
        _idOwner = getIntent().getStringExtra("idOwner");
        listFriendsView = findViewById(R.id.list_friends);
        searchFriendsView = findViewById(R.id.search_friends);
        searchFriendsView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                _listFriends = new ArrayList<>();
                AndroidNetworking.get(API.LOCAL + "user/{username}")
                        .addPathParameter("username",query)
                        .build().getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String code= response.getString("code");
                            String message = response.getString("message");

                            if(code.equals("R_200") && message.equals("OK")){
                                JSONArray array = response.getJSONArray("items");

                                for(int i = 0 ; i < array.length() ; i++){
                                    JSONObject obj = array.getJSONObject(i);
                                    User u = new User(obj.getInt("id"),obj.getString("user_name"),1);
                                    _listFriends.add(u);
                                }
                                adapter = new MyFriendsRecyclerViewAdapter(FriendsActivity.this,_listFriends , Integer.parseInt(_idOwner));
                                listFriendsView.setAdapter(adapter);
                                listFriendsView.setLayoutManager(new LinearLayoutManager(FriendsActivity.this));

                            }
                        } catch (Exception e){
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

    }
}

class MyFriendsRecyclerViewAdapter extends RecyclerView.Adapter<MyFriendsRecyclerViewAdapter.MyFriendsViewHolder> {
    List<User> _datas;
    Context _context;
    int _idOwner;

    public MyFriendsRecyclerViewAdapter(Context context, List<User> datas , int idOwner) {
        _datas = datas;
        _context = context;
        _idOwner = idOwner;
    }

    @NonNull
    @Override
    public MyFriendsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(_context);
        View view = inflater.inflate(R.layout.row_item_user, parent, false);

        return new MyFriendsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyFriendsViewHolder holder, int position) {
        User u = _datas.get(position);
        holder._nameUser.setText(u.getUsername());
        holder._pointUser.setText("");
        holder._idGuest = u.getId();
        holder._idOwner = _idOwner;
    }

    @Override
    public int getItemCount() {
        return _datas.size();
    }



    public class MyFriendsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private int _idGuest;
        private int _idOwner;
        private TextView _nameUser;
        private TextView _pointUser;
        public MyFriendsViewHolder(@NonNull View itemView) {
            super(itemView);
            _nameUser = itemView.findViewById(R.id.name);
            _pointUser = itemView.findViewById(R.id.point);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            ClientControl client = new ClientControl();
            client.addTask(new DataPackage("login",_idOwner + ""));
            client.addTask(new DataPackage("invite",_idOwner + "_" + _idGuest + ""));
            client.execute();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            int len = client.response.size();
            Toast.makeText(_context,len+"",Toast.LENGTH_LONG).show();
        }
    }
}