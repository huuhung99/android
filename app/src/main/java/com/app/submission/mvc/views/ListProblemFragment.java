package com.app.submission.mvc.views;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.app.submission.R;
import com.app.submission.mvc.controllers.API;
import com.app.submission.mvc.controllers.ProblemController;
import com.app.submission.mvc.models.Problem;
import com.app.submission.mvc.models.Resolve;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ListProblemFragment extends Fragment {

    private RecyclerView _listProblemView;
    private ProblemController _problemController;
    private SearchView _searchView;
    private String[] images = new String[3];
    private String[] imagesStar = new String[2];
    private List<Resolve> resolved = new ArrayList<>();
    private MyRecyclerViewAdapter adapter;
    private int _idUser;
    public  ListProblemFragment(){}
    public ListProblemFragment(int idUser){
        _idUser = idUser;
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_list_problem, container, false);

        _listProblemView = root.findViewById(R.id.recycler_problems_view);
        _searchView = root.findViewById(R.id.searchView);
        _searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText.toLowerCase());
                return false;
            }
        });
        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        
        

        _problemController = new ProblemController(this.getContext());
        
        
        _problemController.set_url(API.LOCAL+"problem");

        _problemController.createGetRequestApi(result -> {
            try {
                List<Problem> data = new ArrayList<>();
                JSONObject api = new JSONObject(result);
                JSONArray arr = api.getJSONArray("items");
                for (int i = 0; i < arr.length(); i++) {
                    JSONObject obj = arr.getJSONObject(i);
                    int id = Integer.parseInt(obj.get("id") + "");
                    String title = new String(obj.getString("title").getBytes("ISO-8859-1"), "UTF-8");
                    String topic = new String(obj.getString("topic").getBytes("ISO-8859-1"), "UTF-8");
                    String level = new String(obj.getString("level").getBytes("ISO-8859-1"), "UTF-8");
                    Problem p = new Problem(id, title, topic, level);

                    data.add(p);
                }
                AndroidNetworking.get(API.LOCAL+"resolve/user/{idUser}")
                        .addPathParameter("idUser",_idUser + "")
                        .build().getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String code= response.getString("code");
                            String message = response.getString("message");

                            if(code.equals("R_200") && message.equals("OK")){
                                JSONArray result = response.getJSONArray("items");
                                for(int i = 0 ; i < result.length();i++){
                                    JSONObject obj = result.getJSONObject(i);
                                    int id = obj.getInt("id");
                                    String status = obj.getString("status");
                                    JSONObject problem = obj.getJSONObject("problem");
                                    int idProblem = problem.getInt("id");
                                    Resolve resolve = new Resolve(id ,new Problem(idProblem) , status , _idUser  + "");
                                    resolved.add(resolve);
                                }
                                adapter = new MyRecyclerViewAdapter(getContext(), data , resolved , _idUser);
                                _listProblemView.setAdapter(adapter);
                                _listProblemView.setLayoutManager(new LinearLayoutManager(getContext()));
                            }
                        } catch (Exception e){
                            e.printStackTrace();

                        }
                    }
                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(getContext(),"Error ",Toast.LENGTH_SHORT).show();
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        });

    }
}

class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyRecyclerViewAdapter.MyViewHolder> implements Filterable {
    List<Problem> _datas;
    List<Problem> _datasFilter = new ArrayList<>();
    Context _context;
    List<Resolve> _resolveds;
    int _idUser;

    public MyRecyclerViewAdapter(Context context, List<Problem> datas , List<Resolve> resolveds , int idUser) {
        _datas = datas;
        _datasFilter.addAll(datas);
        _context = context;
        _resolveds = resolveds;
        _idUser = idUser;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(_context);
        View view = inflater.inflate(R.layout.row_item_problem, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Problem p = _datasFilter.get(position);
        holder._titleProblem.setText(Html.fromHtml(p.getTitle()));
        holder._descriptionProblem.setText(Html.fromHtml(p.getTopic()));
        holder._idProblem = p.getId();
        holder._idUser = _idUser;
        String level = p.getLevel();
        if (level.equals("1")) {
            holder._levelProblem.setImageResource(R.drawable.level1);
        } else if (level.equals("2")) {
            holder._levelProblem.setImageResource(R.drawable.level2);
        } else {
            holder._levelProblem.setImageResource(R.drawable.level3);
        }
        boolean flag = false;
        for (Resolve r:
             _resolveds) {
            if(r.getProblem().getId() == p.getId() && r.getStatus().toLowerCase().equals("accepted")){
                flag = true;
                holder._isResolved.setImageResource(R.drawable.gold_star);
                break;
            }
        }
        if(!flag){
            holder._isResolved.setImageResource(R.drawable.siliver_star);
        }
    }

    @Override
    public int getItemCount() {
        return _datasFilter.size();
    }

    @Override
    public Filter getFilter() {
        return filterByName;
    }
    private Filter filterByName = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<Problem> filteredList = new ArrayList<>();
            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(_datas);
            } else {
                String filterPattern = constraint.toString().toLowerCase();
                for (Problem item : _datas) {
                    if (item.getTitle().toLowerCase().contains(filterPattern) || item.getTopic().toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredList;
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            _datasFilter.clear();
            _datasFilter.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView _titleProblem, _descriptionProblem;
        ImageView _levelProblem;
        ImageView _isResolved;
        long _idProblem;
        long _idUser;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            _titleProblem = itemView.findViewById(R.id.titleProblemTxt);
            _descriptionProblem = itemView.findViewById(R.id.descriptionProblemTxt);
            _levelProblem = itemView.findViewById(R.id.im_difficulty);
            _isResolved = itemView.findViewById(R.id.statusProblemImg);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Bundle b = new Bundle();
            b.putString("problemId", _idProblem + "");
            b.putString("userId",_idUser + "");
            Intent i = new Intent(_context, SingleProblemActivity.class);
            i.putExtras(b);
            _context.startActivity(i);
        }
    }
}
