package com.app.submission.mvc.views;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager2.widget.ViewPager2;

import android.os.Bundle;
import android.widget.TableLayout;

import com.app.submission.R;
import com.google.android.material.tabs.TabLayout;

public class SingleProblemActivity extends AppCompatActivity {
    private TabLayout singleProblemTabLayout;
    private ViewPager singleProblemPagerView;
    protected int _idProblem;
    protected  int _idUser;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String problemId = getIntent().getStringExtra("problemId");
        String userId = getIntent().getStringExtra("userId");
        if(!problemId.equals("")){
            _idProblem = Integer.parseInt(problemId);
        }
        if(!userId.equals("")){
            _idUser = Integer.parseInt(userId);
        }

        setContentView(R.layout.activity_single_problem);
        singleProblemTabLayout = findViewById(R.id.single_problem_tab);
        try{
            singleProblemPagerView = findViewById(R.id.single_problem_pager);
        }catch(Exception e){}

        MyPagerViewAdapter adapter = new MyPagerViewAdapter(getSupportFragmentManager(),FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT, _idProblem , _idUser);
        singleProblemPagerView.setAdapter(adapter);
        singleProblemTabLayout.setupWithViewPager(singleProblemPagerView);
    }

}
class MyPagerViewAdapter extends FragmentStatePagerAdapter {

    private int _idProblem;
    private int _idUser;
    public MyPagerViewAdapter(@NonNull FragmentManager fm , int behavior , int idProblem , int idUser) {
        super(fm , behavior);
        _idProblem = idProblem;
        _idUser = idUser;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return new DetailSingleProblemFragment(_idProblem);
            case 1:
                return new CodeSingleProblemFragment(_idProblem , _idUser);
            default:
                return new DetailSingleProblemFragment(_idProblem);
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        String title = "";
        switch (position){
            case 0 :
                title = "Detail";
                break;
            case 1 :
                title = "Code";
                break;
        }
        return title;
    }
}