package com.app.submission.mvc.controllers.services;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.submission.mvc.models.Problem;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class BaseRequest{
    private RequestQueue _requestQueue;
    private String _url;
    public BaseRequest(Context context){
        _requestQueue = Volley.newRequestQueue(context);
    }
    public void set_url(String url){
        _url = url;
    }
    public void createGetRequestApi(final VolleyCallback callback) {
        StringRequest request = new StringRequest(Request.Method.GET, _url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                callback.onSuccess(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                volleyError.printStackTrace();
            }
        });
        _requestQueue.add(request);
    }
}
