package com.app.submission.mvc.views;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.app.submission.R;
import com.app.submission.mvc.controllers.API;
import com.app.submission.mvc.controllers.ClientControl;
import com.app.submission.mvc.models.DataPackage;

import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private static final int REGISTER_REQUEST_CODE = 1;

    private TextView btnForgotPass;
    private TextView btnSignUp;
    private Button btnLogin;
    private EditText edtUsername;
    private EditText edtPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initView();
    }

    private void initView() {
        btnForgotPass= findViewById(R.id.btn_forgotPassword);
        btnSignUp= findViewById(R.id.btn_signUp);
        btnLogin= findViewById(R.id.btn_login);
        btnForgotPass.setOnClickListener(this);
        btnSignUp.setOnClickListener(this);
        btnLogin.setOnClickListener(this);

        edtUsername= findViewById(R.id.edt_register_username);
        edtPassword= findViewById(R.id.edt_register_password);


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_login:
                //=======================================

//                Intent intentMain= new Intent(LoginActivity.this, MainActivity.class);
//                intentMain.putExtra("userId",2 + "");
//                Bundle bundle= new Bundle();
//                bundle.putString("fullName", "Ntx Q");
//                bundle.putString("dateOfBirth", "07/08/9");
//                bundle.putString("phoneNumber", "0987654321");
//                bundle.putString("address","Hanoi");
//                bundle.putString("email", "demo@gmail.com");
//                bundle.putString("sex", "Nam");
//                intentMain.putExtras(bundle);
//                startActivity(intentMain);
//                finish();
                // =====================================

                String username= edtUsername.getText().toString();
                String password= edtPassword.getText().toString();
                if (username.isEmpty() || password.isEmpty()){
                    Toast.makeText(LoginActivity.this, "check username , password", Toast.LENGTH_SHORT).show();
                }
                if(!username.isEmpty() && !password.isEmpty()){

                    AndroidNetworking.get(API.LOCAL+"user/{username}/{password}")
                            .addPathParameter("username", username)
                            .addPathParameter("password", password)
                            .build().getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                String code= response.getString("code");
                                String message = response.getString("message");

                                if(code.equals("R_200") && message.equals("OK")){

                                    JSONObject user = response.getJSONObject("item");
                                    int id = user.getInt("id");



                                    Bundle bundle= new Bundle();
                                    bundle.putString("fullName", user.getString("first_name") +" "+ user.getString("last_name"));
                                    bundle.putString("dateOfBirth", user.getString("date_of_birth"));
                                    bundle.putString("phoneNumber", user.getString("phone"));
                                    bundle.putString("address", user.getString("address"));
                                    bundle.putString("email", user.getString("email"));
                                    bundle.putString("sex", user.getString("sex"));

                                    Intent intentMain= new Intent(LoginActivity.this, MainActivity.class);
                                    intentMain.putExtra("userId",id + "");
                                    intentMain.putExtras(bundle);
                                    startActivity(intentMain);
                                    finish();
                                } else {
                                    Toast.makeText(LoginActivity.this, "Login Failed!", Toast.LENGTH_SHORT).show();
                                }
                            } catch (Exception e){
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Toast.makeText(LoginActivity.this , "Lost Connection To Server",Toast.LENGTH_SHORT).show();
                        }
                    });
                }

                break;
            case R.id.btn_forgotPassword:
                Toast.makeText(this, "Under Construction...", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btn_signUp:
                Intent intentRegister= new Intent(this, RegisterActivity.class);
                startActivityForResult(intentRegister, REGISTER_REQUEST_CODE);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==REGISTER_REQUEST_CODE){
            if(resultCode==RESULT_OK && data!=null){
                edtUsername.setText(data.getStringExtra(RegisterActivity.USERNAME_EXTRA));
                edtPassword.setText(data.getStringExtra(RegisterActivity.PASSWORD_EXTRA));

            }
        }
    }
}