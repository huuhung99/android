package com.app.submission.mvc.models;

public class User {
    private int id;
    private String username;
    private int point;
    public User(){}
    public User(int id , String username , int point){
        this.id = id;
        this.username = username;
        this.point = point;
    }
    public String getUsername(){
        return this.username;
    }
    public int getId(){return this.id;}
}
