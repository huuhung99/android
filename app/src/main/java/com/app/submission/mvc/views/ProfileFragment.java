package com.app.submission.mvc.views;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.app.submission.R;

public class ProfileFragment extends Fragment implements View.OnClickListener {

    private int idUser;
    public ProfileFragment(){};
    public ProfileFragment(int idUser) {
        this.idUser = idUser;
    }

    private TextView fullName;
    private TextView dateOfBirth;
    private TextView address;
    private TextView email;
    private TextView phoneNumber;
    private ImageView avatar;
    private TextView btnLogOut;
    private Button btnFriends;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile, container, false);


    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
        setData();
    }

    private void initView() {
        fullName= getActivity().findViewById(R.id.tv_profile_fullName);
        dateOfBirth= getActivity().findViewById(R.id.tv_profile_dateOFBirth);
        address= getActivity().findViewById(R.id.tv_profile_address);
        email= getActivity().findViewById(R.id.tv_profile_email);
        phoneNumber= getActivity().findViewById(R.id.tv_profile_phoneNumber);
        avatar= getActivity().findViewById(R.id.im_profile_avatar);
        btnLogOut= getActivity().findViewById(R.id.btn_logOut);
        btnFriends = getActivity().findViewById(R.id.btn_list_friends);
        btnFriends.setOnClickListener(this);
        btnLogOut.setOnClickListener(this);
    }
    public void setData(){
        MainActivity main= (MainActivity) getActivity();
        Bundle bundle= main.getMyBundle();
        fullName.setText(bundle.getString("fullName"));
        dateOfBirth.setText("");
        address.setText(bundle.getString("address"));
        email.setText(bundle.getString("email"));
        phoneNumber.setText(bundle.getString("phoneNumber").equals("null")?"":bundle.getString("phoneNumber"));
        if(bundle.getString("sex").equalsIgnoreCase("Nam")){
            avatar.setImageResource(R.drawable.male);
        } else {
            avatar.setImageResource(R.drawable.female);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_logOut :
                Intent intentLogin= new Intent(getActivity(), LoginActivity.class);
                startActivity(intentLogin);
                getActivity().finish();
                break;
            case R.id.btn_list_friends:
                Intent intentFriends= new Intent(getActivity(), FriendsActivity.class);
                intentFriends.putExtra("idOwner",idUser+"");
                startActivity(intentFriends);

                break;
        }
    }
}
