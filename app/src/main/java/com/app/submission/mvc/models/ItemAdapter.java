package com.app.submission.mvc.models;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.app.submission.R;
import com.app.submission.mvc.controllers.API;

import org.json.JSONObject;

import java.util.ArrayList;

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ItemHolder> {

    private LayoutInflater inflater;
    private ArrayList<Problem> list;
    private ItemClickListener listener;

    public ItemAdapter(LayoutInflater inflater) {
        this.inflater = inflater;
    }

    public ArrayList<Problem> getList() {
        return list;
    }

    public void setList(ArrayList<Problem> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public void setListener(ItemClickListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public ItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= inflater.inflate(R.layout.row_item_problem, parent, false);
        return new ItemHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemHolder holder, int position) {
        holder.bindView(list.get(position));

        if (listener != null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(list.get(position));
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return list==null? 0:list.size();
    }

    public class ItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView title;
        private TextView desc;
        private ImageView imageDif;
        private ConstraintLayout layoutBackground;
        public ItemHolder(@NonNull View itemView) {
            super(itemView);
            title= itemView.findViewById(R.id.titleProblemTxt);
            desc= itemView.findViewById(R.id.descriptionProblemTxt);
            imageDif= itemView.findViewById(R.id.im_difficulty);
            layoutBackground= itemView.findViewById(R.id.layout_background);
            itemView.setOnClickListener(this);
        }
        public void bindView(Problem problem){
            title.setText(problem.getTitle()+"");
            desc.setText(problem.getTopic());
            if(problem.getLevel().equals("1")){
                imageDif.setImageResource(R.drawable.level1);
            } else if(problem.getLevel().equals("2")){
                imageDif.setImageResource(R.drawable.level2);
            } else{
                imageDif.setImageResource(R.drawable.level3);
            }
            if(!problem.getSolved().equals("")){
                if(problem.getSolved().equals("Accepted")){
                    layoutBackground.setBackgroundColor(Color.rgb(166, 234, 86));
                } else {
                    layoutBackground.setBackgroundColor(Color.rgb(255,99,86));
                }
            }
        }

        @Override
        public void onClick(View v) {
//            AndroidNetworking.get(API.LOCAL+"resolve/{id}")
//                    .addPathParameter("id","15")
//                    .build().getAsJSONObject(new JSONObjectRequestListener() {
//                @Override
//                public void onResponse(JSONObject response) {
//                    try {
//                        String code = response.getString("code");
//                        String message = response.getString("message");
//                        if(code.equals("R_200") && message.equals("OK")){
//
//                        }
//                    } catch (Exception e) {
//                    }
//                }
//
//                @Override
//                public void onError(ANError anError) {
//
//                }
//            });
        }
    }
    public interface ItemClickListener {
        void onItemClick(Problem problem);
    }
}
