package com.app.submission.mvc.models;

public class Language {
    private int id;
    private String name;
    private boolean is_archived;

    public Language() {
    }

    public Language(int id, String name, boolean is_archived) {
        this.id = id;
        this.name = name;
        this.is_archived = is_archived;
    }

    public int getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isIs_archived() {
        return is_archived;
    }

    public void setIs_archived(boolean is_archived) {
        this.is_archived = is_archived;
    }
}
