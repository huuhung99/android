package com.app.submission.mvc.models;

import java.io.Serializable;

public class Problem implements Serializable {
    private int id;
    private String input;
    private String output;
    private float cpu_time_limit;
    private float cpu_extra_time;
    private int memory_limit;
    private int max_file_size;
    private boolean active;
    private String code;
    private String title;
    private String topic;
    private String content;
    private String level;

    private String solved= "";

    public Problem() {
    }

    public Problem(int id, String input, String output, Float cpu_time_limit, Float cpu_extra_time,
                   Integer memory_limit, Integer max_file_size, boolean active, String code,
                   String title, String topic, String content, String level) {
        this.id = id;
        this.input = input;
        this.output = output;
        this.cpu_time_limit = cpu_time_limit;
        this.cpu_extra_time = cpu_extra_time;
        this.memory_limit = memory_limit;
        this.max_file_size = max_file_size;
        this.active = active;
        this.code = code;
        this.title = title;
        this.topic = topic;
        this.content = content;
        this.level = level;
    }
    public Problem(int id , String title , String topic , String level){
        this.id = id;
        this.title = title;
        this.topic = topic;
        this.level = level;
    }
    public Problem(int id){
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public Float getCpu_time_limit() {
        return cpu_time_limit;
    }

    public void setCpu_time_limit(Float cpu_time_limit) {
        this.cpu_time_limit = cpu_time_limit;
    }

    public Float getCpu_extra_time() {
        return cpu_extra_time;
    }

    public void setCpu_extra_time(Float cpu_extra_time) {
        this.cpu_extra_time = cpu_extra_time;
    }

    public Integer getMemory_limit() {
        return memory_limit;
    }

    public void setMemory_limit(Integer memory_limit) {
        this.memory_limit = memory_limit;
    }

    public Integer getMax_file_size() {
        return max_file_size;
    }

    public void setMax_file_size(Integer max_file_size) {
        this.max_file_size = max_file_size;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getSolved() {
        return solved;
    }

    public void setSolved(String solved) {
        this.solved = solved;
    }
}
