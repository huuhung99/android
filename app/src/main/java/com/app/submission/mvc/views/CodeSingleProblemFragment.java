package com.app.submission.mvc.views;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.ListFragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.app.submission.R;
import com.app.submission.mvc.controllers.API;
import com.app.submission.mvc.controllers.LanguageController;
import com.app.submission.mvc.controllers.ProblemController;
import com.app.submission.mvc.models.Language;
import com.app.submission.mvc.models.Problem;
import com.app.submission.mvc.models.Resolve;
import com.app.submission.mvc.models.request.ResolveRequest;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class CodeSingleProblemFragment extends Fragment {
    private EditText _sourceCode;
    private TextInputLayout _languagesDropdown;
    private AutoCompleteTextView _languageItems;
    private LanguageController _languageController;
    private Button _submit;
    private List<Language> languages = new ArrayList<>();
    private String token = "";
    private int _idLanguage = 0;
    private int _idProblem;
    private int _idUser;
    private Problem _problem = new Problem();
    public CodeSingleProblemFragment(int idProblem , int idUser){
        _idUser = idUser;
        _idProblem = idProblem;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_code_single_problem, container, false);
        AndroidNetworking.get(API.LOCAL+"problem/" + _idProblem)
                .build().getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    String code= response.getString("code");
                    String message = response.getString("message");

                    if(code.equals("R_200") && message.equals("OK")){
                        JSONObject obj = response.getJSONObject("item");
                        _problem.setInput(obj.getString("input"));
                        _problem.setOutput(obj.getString("output"));
                        _problem.setMemory_limit(2048);
                    }
                }catch (Exception e){e.printStackTrace();};

            }

            @Override
            public void onError(ANError anError) {

            }
        });


        AndroidNetworking.get(API.LOCAL+"resolve/user/{idUser}/{idProblem}")
                .addPathParameter("idUser",_idUser + "")
                .addPathParameter("idProblem", _idProblem + "")
                .build().getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    String code= response.getString("code");
                    String message = response.getString("message");

                    if(code.equals("R_200") && message.equals("OK")){
                        JSONArray result = response.getJSONArray("items");
                        if(result.length() >= 1){
                            JSONObject obj = result.getJSONObject(result.length() - 1);
                            token = obj.getString("token");
                            if(!token.equals("")){
                                AndroidNetworking.get("https://judge0-ce.p.rapidapi.com/submissions/batch?tokens={token}&base64_encoded=false&fields=*")
                                        .addHeaders("content-type","application/json")
                                        .addPathParameter("token",token)
                                        .addHeaders("x-rapidapi-key", "1ea4efebfamshf0a205ab9e87a07p166590jsn5360d16176c4")
                                        .addHeaders("x-rapidapi-host", "judge0-ce.p.rapidapi.com")
                                        .build().getAsJSONObject(new JSONObjectRequestListener() {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        try {
                                            JSONArray result = response.getJSONArray("submissions");
                                            JSONObject obj = result.getJSONObject(0);
                                            _sourceCode.setText(obj.getString("source_code"));

                                        } catch (Exception e){
                                            e.printStackTrace();

                                        }
                                    }
                                    @Override
                                    public void onError(ANError anError) {

                                    }
                                });

                            }
                            JSONObject language = obj.getJSONObject("language");
                            _idLanguage = language.getInt("id");
                            String languageText = language.getString("name");
                            if(_idLanguage != 0){
                                _languageItems.setText(languageText);
                            }
                        }
                    }
                } catch (Exception e){
                    e.printStackTrace();

                }
            }
            @Override
            public void onError(ANError anError) {

            }
        });

        _languagesDropdown = root.findViewById(R.id.languages);
        _languageItems = root.findViewById(R.id.autoCompleteTextView);
        _sourceCode = root.findViewById(R.id.editTextTextMultiLine);
        _submit = root.findViewById(R.id.button4);
        _submit.setOnClickListener(v -> {
            ResolveRequest request = new ResolveRequest();
            request.setAccount_id(_idUser + "");
            request.setProblem_id(_idProblem);
            for (Language item:
                 languages) {
                if(item.getName().equals(_languageItems.getText().toString())){
                    request.setLanguage_id(item.getId());
                    break;
                }

            }
            request.setSource_code(_sourceCode.getText().toString());
            request.setStdin(_problem.getInput());
            request.setMemory_limit(_problem.getMemory_limit());

            Toast.makeText(getContext() , "Waiting ...." , Toast.LENGTH_SHORT).show();

            JSONObject body = null;
            try {
                body = new JSONObject(new Gson().toJson(request));
                AndroidNetworking.post(API.LOCAL + "resolve")
                        .addHeaders("content-type","application/json")
                        .addJSONObjectBody(body)
                        .build().getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String code= response.getString("code");
                            String message = response.getString("message");

                            if(code.equals("R_201") && message.equals("Create")){
                                Toast.makeText(getContext() , "Send Success !" , Toast.LENGTH_SHORT).show();
                                AndroidNetworking.get(API.LOCAL+"users/" + _idUser)
                                        .build().getAsJSONObject(new JSONObjectRequestListener() {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        try {
                                                Bundle bundle= new Bundle();
                                                bundle.putString("fullName",  response.getString("first_name") + " " + response.getString("last_name"));
                                                bundle.putString("dateOfBirth","");
                                                bundle.putString("phoneNumber", response.getString("phone"));
                                                bundle.putString("address", response.getString("address"));
                                                bundle.putString("email", response.getString("email"));
                                                bundle.putString("sex", response.getString("sex"));

                                                Intent intentMain= new Intent(getActivity(), MainActivity.class);
                                                intentMain.putExtra("userId",_idUser + "");
                                                intentMain.putExtra("fromCode",true);
                                                intentMain.putExtras(bundle);
                                                startActivity(intentMain);
                                                getActivity().finish();

                                        }catch(Exception e){
                                            e.printStackTrace();
                                        }
                                    }
                                    @Override
                                    public void onError(ANError anError) {

                                    }
                                });
                            }
                        } catch (Exception e){
                            e.printStackTrace();

                        }
                    }
                    @Override
                    public void onError(ANError anError) {

                    }
                });
            } catch (JSONException e) {
                e.printStackTrace();
            }

        });
        _languageController = new LanguageController(this.getContext());
        _languageController.set_url(API.LOCAL+"language");
        _languageController.createGetRequestApi(result -> {
            try {
                JSONObject api = new JSONObject(result);
                JSONArray arr = api.getJSONArray("items");
                String[] languagesText = new String[arr.length()];
                for (int i = 0; i < arr.length(); i++) {
                    JSONObject obj = arr.getJSONObject(i);
                    languages.add(new Language(obj.getInt("id") , obj.getString("name"),false ));
                    languagesText[i] = obj.getString("name");
                }
                if(_idLanguage == 0){
                    _languageItems.setText(languagesText[0]);
                }
                ArrayAdapter adapter = new ArrayAdapter(requireContext(),R.layout.dropdown_item_language,languagesText);
                _languageItems.setAdapter(adapter);
            } catch (Exception e) {
                System.out.println(e.getStackTrace());
            }
        });

        return root;
    }

}