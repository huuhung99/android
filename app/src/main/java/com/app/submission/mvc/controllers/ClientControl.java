/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.submission.mvc.controllers;

import android.os.AsyncTask;

import com.app.submission.mvc.models.DataPackage;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class ClientControl extends AsyncTask<Void,Void,Void> {
    private Socket mySocket;
    private final String IP_ADDRESS = "192.168.1.9";
    private final int PORT = 8889;
    private List<DataPackage> request;
    public List<DataPackage> response;

    public ClientControl(){
        request = new ArrayList<>();
        response = new ArrayList<>();
    }
    @Override
    protected Void doInBackground(Void... voids) {
        try {
            for(DataPackage item : this.request){
                mySocket = new Socket(IP_ADDRESS , PORT);
                sendData(item);
                System.out.println(mySocket.getLocalPort());
                ObjectInputStream ois = new ObjectInputStream(mySocket.getInputStream());
                Object o = ois.readObject();
                if (o instanceof DataPackage) {
                    DataPackage result = (DataPackage) o;
                    response.add(result);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void addTask(DataPackage task){
        this.request.add(task);
    }

    public void resetTask(){
        this.request = new ArrayList<>();
    }

    public boolean sendData(DataPackage dp) {
        try {
            ObjectOutputStream oos = new ObjectOutputStream(mySocket.getOutputStream());
            oos.writeObject(dp);
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    public boolean closeConnection() {
        try {
            mySocket.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
    }
}
